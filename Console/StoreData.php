<?php

namespace Console;

use Core\Database;
use Exceptions\LogError;
use Model\Source;
use Utils\SaveXMLData;

class StoreData
{

    public LogError $logError;

    public function __construct(LogError $logError )
    {
        $this->logError = $logError;
    }

    /**
     * @throws LogError
     */
    public function execute($sourceFile, $destinationStorage): void
    {
        $sourceFileExtension = pathinfo($sourceFile, PATHINFO_EXTENSION);
        $db = $this->createStorage($destinationStorage);

        switch ($sourceFileExtension) {
            case Source::XML_SOURCE:
                $saveXmlDataClass = new SaveXMLData();
                $saveXmlDataClass->fillXmlIntoStorage($sourceFile, $db);
                break;
            default:
                throw new LogError('Not supported value for source path.');
        }
    }

    /**
     * @throws LogError
     */
    public function createStorage($destinationStorage): Database
    {
        switch ($destinationStorage) {
            case Source::SQLITE_STORAGE:
            case Source::MYSQL_STORAGE:

                //Build Database
                $databaseName = 'kauf_' . $destinationStorage;
                $db = new Database($destinationStorage, [
                    'host' => 'localhost',
                    'port' => 3306,
                    'dbname' => $databaseName,
                    'charset' => 'utf8mb4'
                ]);
                break;
            default:
                throw new LogError('Not supported value for destination storage.');

        }
        return $db;
    }

}
