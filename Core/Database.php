<?php

namespace Core;

use Exceptions\LogError;
use PDO;

class Database
{
    public $connection;
    public $statement;

    public function __construct( $type, $config, $username = 'root', $password = '')
    {
        $dsn = $type . ':' . http_build_query($config, '', ';');

        $this->connection = new PDO($dsn, $username, $password, [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
    }


    public function addColumnsToTable($arrayContent): void
    {
        $query = "CREATE TABLE IF NOT EXISTS items ( ";
        foreach($arrayContent as $value)
        {
            $query .= $value . " " . "TEXT" ;
            if (end($arrayContent) !== $value) {
                $query .= ", ";
            }
        }
        $query .= " ); ";

        $this->query($query);
    }


    public function insertData($record)
    {
        $columns = [];
        $values = [];
        foreach ($record->children() as $field) {
            $fieldName = $field->getName();
            $columns[] = $fieldName;
            $values[] = (string) $field;
        }
        $placeholders = implode(', ', array_fill(0, count($columns), '?'));
        $sql = "INSERT INTO items (" . implode(', ', $columns) . ") VALUES ($placeholders)";

        $this->query($sql, $values);

    }
    private function query($query, $params = [])
    {
        $this->statement = $this->connection->prepare($query);
        $this->statement->execute($params);

        return $this;
    }

}
