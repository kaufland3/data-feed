<?php

namespace Model;

class Source
{
    //Here you can add the different sources or storages

    //File Extensions
    const string XML_SOURCE = 'xml';

    //Storage
    const string SQLITE_STORAGE = 'sqlite';
    const string MYSQL_STORAGE = 'mysql';
}
