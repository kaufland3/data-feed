<?php

namespace Exceptions;

use Exception;

class LogError extends Exception
{
    public function logMessage($message): void
    {
        $timestamp = date('Y-m-d H:i:s');
        $logMessage = "$timestamp: $message\n";
        file_put_contents('logs/error.log', $logMessage, FILE_APPEND);

    }
}
