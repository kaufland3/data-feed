# Coding Task – Data Feed

### Goal
We aim to develop a command-line program that processes a local XML file (`feed.xml`) and transfers its data to a database of your choice (e.g., SQLite).
You have the freedom to utilize any library or framework you're comfortable with, but please implement it in PHP.

### Specifications
- The current code inserts data from a Xml file into a database (mysql or sqlite).
- Can be scalable to support other types of storage or file type by adding other cases in file ''Kaufland_test/Console/StoreData.php'' and the necessary logic to prepare the data to the storage preferred. 
- Errors encountered during execution are logged under ''Kaufland_test/logs/error.log''.
- The application should be thoroughly tested.


## How to run this code
### Prerequisites
- Have PHP installed in your computer.
- In the root folder run **composer install**

### Command line
- **php store_data.php <source_file_path> <destination_storage>**
  - <source_file_path>: the path of the file you would like to store the data from. File types supported: XML.
  - <destination_storage>: the type of storage that you would like to use. Storage supported: mysqli, mysql.
- **phpunit**
  - This command in the root folder will run the tests