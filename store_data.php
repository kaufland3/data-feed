<?php

require_once __DIR__ . '/vendor/autoload.php';

use Exceptions\LogError;
use Console\StoreData;

if ($argc < 3) {
    echo "Usage: php store_data.php <source_file_path> <destination_storage>\n";
    echo "<source_file_path>: the path of the file you would like to store the data from. File types supported: XML, JSON, txt \n";
    echo "<destination_storage>: the type of storage that you would like to use. Storage supported: mysqli, mysql \n";
    exit(1);
}

//Data from the command
$sourceFilePath = $argv[1];
$destinationStorage = $argv[2];

// Execute the command
$logError = new LogError();
$command = new StoreData($logError);
$command->execute($sourceFilePath, $destinationStorage);
