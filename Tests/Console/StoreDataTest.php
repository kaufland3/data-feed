<?php

namespace tests\Console;

use Core\Database;
use PHPUnit\Framework\TestCase;
use Console\StoreData;
use Exceptions\LogError;
use Model\Source;
use Utils\SaveXMLData;

class StoreDataTest extends TestCase
{
    private StoreData $storeData;

   protected function setUp(): void
    {
        $logErrorMock = $this->getMockBuilder(LogError::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->storeData = new StoreData($logErrorMock);
    }

    public function testExecuteWithXMLSource(): void
    {
        $saveXmlDataMock = $this->getMockBuilder(SaveXMLData::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertNotNull($saveXmlDataMock);
        $this->assertInstanceOf(SaveXMLData::class, $saveXmlDataMock);

        $saveXmlDataMock->expects($this->once())
            ->method('fillXmlIntoStorage')
            ->with(
                $this->equalTo('feed.xml'),
                $this->isInstanceOf(Database::class)
            );

        $this->storeData->execute('feed.xml', Source::SQLITE_STORAGE);
    }

    /**
     * @dataProvider dataProviderForExecuteWithUnsupportedSources
     */

    public function testExecuteWithUnsupportedSources($sourceFile, $destinationStorage, $expectedExceptionMessage): void
    {
        $this->expectException(LogError::class);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $this->storeData->execute($sourceFile, $destinationStorage);
    }

    public static function dataProviderForExecuteWithUnsupportedSources(): array
    {
        return [
            ['example.json', 'sqlite', 'Not supported value for source path.'],
            ['feed.xml', 'unsupported_storage', 'Not supported value for destination storage.'],
        ];
    }

}
