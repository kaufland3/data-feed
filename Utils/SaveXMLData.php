<?php

namespace Utils;

use Core\Database;
use Exceptions\LogError;

class SaveXMLData
{

    public LogError $logError;

    public function __construct(LogError $logError)
    {
        $this->logError = $logError;
    }

    /**
     * @throws LogError
     */
    public function fillXmlIntoStorage(string $sourceFile, Database $db): void
    {
        try {
            $xml = simplexml_load_file($sourceFile);

            if (empty($xml)) {
                throw new LogError('There is no data in the file provided.');
            }
            //get columns to build the table of the database
            $columns = $this->getDBColumns($xml);

            $db->addColumnsToTable($columns);

            // Insert data into the table
            foreach ($xml->children() as $record) {
                $db->insertData($record);
            }

        } catch (LogError $e) {
            $this->logError->logMessage($e->getMessage());
            throw new LogError('There was an error saving the data.');
        }

    }

    public function getDBColumns($items): array
    {
        // Create table based on XML structure
        // Assuming all the items in the catalog will have the same properties
        $firstChild = [];
        foreach ($items as $item) {
            $firstChild = (array)$item;
            break;
        }
        return array_keys($firstChild);

    }

}
